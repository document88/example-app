<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\User;

class ArticlesController extends Controller
{
    // 控制器創建時自動呼叫 只有登入後才能訪問 例外()
    public function __construct()
    {
        $this->middleware(middleware: 'auth')->except(['index', 'show']);
    }
    // 文章列表
    public function index()
    {
        // 檢查參數中是否包含edit
        $hasEdit = request()->has('edit');
        // 檢查是否有搜尋參數
        $hasSearch = request('search');
        // 如果有edit 顯示你的文章
        if ($hasEdit) {
            $articles = User::find(auth()->id())->articles()->orderBy('id', 'desc');
        } else {
            // 否則 顯示所有人已發布的文章列表
            $articles = Article::where('state', '=', 'published')->orderBy('id', 'desc')->with('user');
        }
        // 如果有搜尋參數 進行搜尋
        if ($hasSearch) {
            $articles->where('title', 'like', '%' . $hasSearch . '%')
                ->orWhere('content', 'like', '%' . $hasSearch . '%');
        }
        // 使用 paginate() 方法取得分頁結果
        $articles = $articles->paginate(8);
        return view('articles/index', ['articles' => $articles, 'hasSearch' => $hasSearch]);
    }
    // 詳細文章
public function show($id)
{
    // 使用 auth()->id() 獲取當前登入者的 ID
    $article = Article::where('id', $id)
                      ->where(function ($query) {
                          $query->where('state', 'published')
                                ->orWhere(function ($query) {
                                    $query->where('state', 'draft')
                                          ->where('user_id', auth()->id());
                                });
                      })
                      ->first();
        if (!$article) {
            abort(404, '文章不存在');
        }
        return view('articles.show', ['article' => $article]);
    }
    // 新增文章-表單視圖
    public function create()
    {
        return view('articles/create');
    }
    // 新增文章
    public function store(Request $request)
    {
        $content = $request->validate([
            'title' => 'required|max:100',
            'content' => 'required|max:500',
            'state' => ['required', 'in:draft,published'],
        ]);
        User::find(auth()->id())->articles()->create([
            'title' => $content['title'],
            'content' => $content['content'],
            'state' => $content['state'],
        ]);
        return redirect()->route('articles.index')->with('notice', '文章新增成功');
    }
    // 編輯文章-表單視圖
    public function edit($id)
    {
        $article = auth()->user()->articles->find($id);
        return view('articles.edit', ['article' => $article]);
    }
    // 編輯文章
    public function update(Request $request, $id)
    {
        $article = auth()->user()->articles->find($id);
        $content = $request->validate([
            'title' => 'required|max:100',
            'content' => 'required|max:500',
            'state' => ['required', 'in:draft,published'],
        ]);
        $article->update([
            'title' => $content['title'],
            'content' => $content['content'],
            'state' => $content['state'],
        ]);
        return redirect()->route('articles.index')->with('notice', '文章編輯成功');
    }
    // 刪除文章
    public function destroy($id)
    {
        $article = auth()->user()->articles->find($id);
        $article->delete();
        return redirect()->route('articles.index')->with('notice', '文章刪除成功');
    }
}
