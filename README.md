# Laravel 文章功能網站

## 初始設置

請按照以下步驟進行初始設置：

1. **安裝專案依賴：**
   ```bash
   composer install
   npm install
   ```

2. **設定資料庫連線：**
   - 複製 .env.example 檔案並生成 .env 檔案：
   ```bash
   cp .env.example .env
   ```
   - 生成應用程式金鑰：
   ```bash
   php artisan key:generate
   ```
   - 打開 .env 檔案，填寫資料庫連線資訊：
   ```
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=your_database_name
   DB_USERNAME=your_database_user
   DB_PASSWORD=your_database_password
   ```

3. **執行資料庫遷移：**
   ```bash
   php artisan migrate
   ```

4. **設定額外配置：**
   ```bash
   npm install -D vite
   npm run build
   ```

5. **運行專案：**
   ```bash
   php artisan serve
   ```

6. **訪問專案：**
   透過瀏覽器訪問http://localhost:8000

## 專案說明

這個 Laravel 專案旨在創建一個文章功能的網站，包括文章列表顯示、新增文章、編輯文章、刪除文章等功能。同時，利用 Laravel 內置的會員功能快速創建會員的登入與註冊：
```bash
composer require laravel/jetstream 
php artisan jetstream:install livewire
```
