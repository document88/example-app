@extends('layouts/article')

@section('main')

      <div class="card border-success mb-3 container" style="padding: 0;">
        <div class="card-header text-success" style="font-size: 1.5em;">{{ $article->title }}</div>
        <div class="card-body">
          <h7 class="card-title">作者: {{ $article->user->name }}&nbsp;/&nbsp;發布日期: {{ $article->created_at->format('Y-m-d H:i:s') }}</h7>
          <div class="card-text mt-2">{!! nl2br(e($article->content)) !!}</div>
        </div>
      </div>

@endsection