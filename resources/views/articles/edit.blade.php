@extends('layouts/article')

@section('main')

<!-- 錯誤提示 -->
@if($errors->any())
@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <i class="bi bi-exclamation-triangle-fill"></i>&nbsp;{{ $error }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endforeach
@endif

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <h2 class="mb-4">編輯文章</h2>
      <form action="{{ route('articles.update', ['article' => $article->id]) }}" method="post">
        @method('PATCH')
        @csrf
        <div class="mb-3">
          <label for="title" class="form-label">標題:</label>
          <input type="text" class="form-control" id="title" name="title" value="{{ $article->title }}" maxlength="100" required>
        </div>
        <div class="mb-3">
          <label for="content" class="form-label">文章內容:</label>
          <textarea class="form-control" id="content" name="content" rows="3" maxlength="500" required>{{ $article->content }}</textarea>
        </div>
        <div class="mb-3 d-flex flex-row align-items-center">
          <span class="me-3">文章狀態:</span>
          <div class="form-check me-3">
            <input class="form-check-input" type="radio" name="state" id="state1" value="published" {{ $article->state == 'published' ? 'checked' : ''}}>
            <label class="form-check-label" for="state1">
              發布
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="state" id="state2" value="draft" {{ $article->state == 'draft' ? 'checked' : ''}}>
            <label class="form-check-label" for="state2">
              草稿
            </label>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">確認編輯</button>
      </form>
    </div>
  </div>
</div>
@endsection