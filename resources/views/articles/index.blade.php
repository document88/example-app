@extends('layouts/article')

@section('main')
<!-- 新增成功提示框 -->
@if(session('notice'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="bi bi-check-circle-fill"></i>&nbsp;{{ session('notice') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<!-- 如果沒有edit的參數就顯示所有文章的文章列表 -->
@if(!request()->has('edit'))
<div class="table-responsive container">
    <table class="table row mx-auto table-striped table-hover">
        <thead>
            <tr class="row">
                <th class="col-md-8 col-6">文章</th>
                <th class="col-md-2 col-2">作者</th>
                <th class="col-md-2 col-4">發布時間</th>
            </tr>
        </thead>
        @if($articles->isEmpty())
        <tbody>
            <tr class="row">
                <td class="col-md-12 col-12" style="text-align: center;">暫無文章</td>
            </tr>
        </tbody>
        @else
        <tbody>
            @foreach($articles as $article)

            <tr class="row">
                <td class="col-md-8 col-6">
                    <a href="{{ route('articles.show', $article) }}">
                        <div class="text-truncate">
                            <strong style="font-size: 1.1em">{{ $article->title }}</strong><br>
                            {{ $article->content }}
                        </div>
                    </a>
                </td>
                <td class="col-md-2 col-2">
                    <div style="line-height:3;">{{ $article->user->name }}</div>
                </td>
                <td class="col-md-2 col-4">
                    <div class="text-truncate" style="line-height:3;">{{ $article->created_at }}</div>
                </td>
            </tr>
            @endforeach
        </tbody>
        @endif
    </table>
</div>
<!-- 分頁按鈕 -->
<div class="pagination">
    <ul>
        @if ($articles->onFirstPage())
        <li class="disabled"><span>&laquo;</span></li>
        @else
        <li><a href="{{ $articles->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        @php
        // 計算起始和結束頁數
        $start = max(1, $articles->currentPage() - 2);
        $end = min($articles->lastPage(), $articles->currentPage() + 2);
        @endphp

        @if ($start > 1)
        <li><a href="{{ $articles->url(1) }}">1</a></li>
        @if ($articles->currentPage() - 3 > 1)
        <li><span style="border: 0">...</span></li>
        @endif
        @endif

        @for ($i = $start; $i <= $end; $i++) @if ($i==$articles->currentPage())
            <li class="active"><span>{{ $i }}</span></li>
            @else
            <li><a href="{{ $articles->url($i) }}">{{ $i }}</a></li>
            @endif
            @endfor

            @if ($end < $articles->lastPage())
                @if ($articles->lastPage() - $articles->currentPage() > 2)
                <li><span style="border: 0">...</span></li>
                @endif
                <li><a href="{{ $articles->url($articles->lastPage()) }}">{{ $articles->lastPage() }}</a></li>
                @endif

                @if ($articles->hasMorePages())
                <li><a href="{{ $articles->nextPageUrl() }}" rel="next">&raquo;</a></li>
                @else
                <li class="disabled"><span>&raquo;</span></li>
                @endif
    </ul>
</div>
<!-- 否則有edit顯示編輯文章的文章列表 -->
@else
<div class="table-responsive container">
    <table class="table row mx-auto table-striped table-hover">
        <thead>
            <tr class="row">
                <th class="col-md-6 col-4">文章</th>
                <th class="col-md-2 col-2">狀態</th>
                <th class="col-md-2 col-4">發布時間</th>
                <th class="col-md-2 col-2">操作</th>
            </tr>
        </thead>
        @if($articles->isEmpty())
        <tbody>
            <tr class="row">
                <td class="col-md-12 col-12" style="text-align: center;">暫無文章</td>
            </tr>
        </tbody>
        @else
        <tbody>
            @foreach($articles as $article)
            <tr class="row">
                <td class="col-md-6 col-4">
                    <a href="{{ route('articles.show', $article) }}">
                        <div class="text-truncate">
                            <strong style="font-size: 1.1em">{{ $article->title }}</strong><br>
                            {{ $article->content }}
                        </div>
                    </a>
                </td>
                <td class="col-md-2 col-2">
                    <div style="line-height:3;">{{ $article->state === 'published' ? "發布" : "草稿" }}</div>
                </td>
                <td class="col-md-2 col-4">
                    <div class="text-truncate" style="line-height:3;">{{ $article->created_at }}</div>
                </td>
                <td class="col-md-2 col-2 index_set_box">
                    <a href="{{ route('articles.edit', ['article' => $article]) }}">
                        <button type="button" class="btn btn-success index_set">編輯</button>
                    </a>
                    <div style="display: inline-block;">
                    <form action="{{ route('articles.destroy', $article) }}" method="post">
                        @method('DELETE')
                        @csrf
                    <button type="submit" class="btn btn-danger index_set">刪除</button>
                </form></div>
                </td>
            </tr>
            @endforeach
        </tbody>
        @endif
    </table>
</div>
<!-- 分頁按鈕 -->
<div class="pagination">
    <ul>
        <!-- 前往上一頁 -->
        @if ($articles->onFirstPage())
        <li class="disabled"><span>&laquo;</span></li>
        @else
        <li><a href="{{ $articles->previousPageUrl() }}{{ $articles->previousPageUrl() ? '&' : '?' }}edit=true" rel="prev">&laquo;</a></li>
        @endif

        @php
        // 計算起始和結束頁數
        $start = max(1, $articles->currentPage() - 2);
        $end = min($articles->lastPage(), $articles->currentPage() + 2);
        @endphp

        <!-- 跳到第1頁 -->
        @if ($start > 1)
        <li><a href="{{ $articles->url(1) }}{{ $articles->url(1) ? '&' : '?' }}edit=true">1</a></li>
        <li><span style="border: 0">...</span></li>
        @endif

        <!-- 只要不是當前頁面都產生a標籤 -->
        @for ($i = $start; $i <= $end; $i++) @if ($i==$articles->currentPage())
            <li class="active"><span>{{ $i }}</span></li>
            @else
            <li><a href="{{ $articles->url($i) }}{{ $articles->url($i) ? '&' : '?' }}edit=true">{{ $i }}</a></li>
            @endif
            @endfor

            <!-- 跳到最後一頁 -->
            @if ($end < $articles->lastPage())
                <li><span style="border: 0">...</span></li>
                <li><a href="{{ $articles->url($articles->lastPage()) }}{{ $articles->url($articles->lastPage()) ? '&' : '?' }}edit=true">{{ $articles->lastPage() }}</a></li>
                @endif

                <!-- 前往下一頁 -->
                @if ($articles->hasMorePages())
                <li><a href="{{ $articles->nextPageUrl() }}{{ $articles->nextPageUrl() ? '&' : '?' }}edit=true" rel="next">&raquo;</a></li>
                @else
                <li class="disabled"><span>&raquo;</span></li>
                @endif
    </ul>
</div>
@endif
@endsection