<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-icons.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/article.css') }}">
  <title>Example</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('home') }}">Laravel</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="{{ route('home') }}">首頁</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->is('articles') && !request()->has('edit') ? 'active' : '' }}" href="{{ route('articles.index') }}">文章列表</a>
          </li>
          <!-- 這裡的內容只會在使用者已登入時顯示 -->
          @auth
          <li class="nav-item">
            <a class="nav-link {{ request()->is('articles/create') ? 'active' : '' }}" href="{{ route('articles.create') }}">新增文章</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->has('edit') ? 'active' : '' }}" href="{{ route('articles.index', ['edit' => true]) }}">你的文章</a>
          </li>
          @endauth
        </ul>

        <!-- 登入與註冊按鈕 -->
        @guest
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">登入</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">註冊</a>
          </li>
        </ul>
        @else
        <!-- 如果使用者已經登入，顯示使用者名稱和登出按鈕 -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <span class="nav-link">您好, {{ Auth::user()->name }}</span>
          </li>
          <li class="nav-item">
            <form action="{{ route('logout') }}" method="POST">
              @csrf
              <button type="submit" class="nav-link btn btn-link">登出</button>
            </form>
          </li>
        </ul>
        @endguest
        <!-- 搜尋表單 -->
        <form class="d-flex search_form" action="{{ route('articles.index') }}" method="get">
          <input class="form-control me-2" type="search" name="search" placeholder="搜尋文章" aria-label="Search" value="{{ isset($hasSearch) && $hasSearch ? $hasSearch : ''}}">
          <button class="btn btn-outline-success" type="submit"><i class="bi bi-search"></i></button>
        </form>
      </div>
    </div>
  </nav>
  <main>
    @yield('main')
  </main>
  <footer class="bg-dark text-light text-center py-3">
    <p>&copy; <?php echo date("Y"); ?> wenhao。0800-888-888</p>
  </footer>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- <script src="{{ asset('js/app.js') }}"></script> -->
</body>

</html>